# moneyspace-php

Moneyspace client PHP Api. The api follows the following practices:

- namespaced under Moneyspace\Api
- call $api->class->function() to access the api
- options are passed as an array instead of multiple arguments wherever possible
- All request and responses are communicated over JSON

# Installation

- If you are not using composer, download the latest release from [the releases section]
After that include `Moneyspace.php` in your application and you can use the API as usual.

# Usage

```php

require_once 'Require.php';

use Moneyspace\Api\Api;

$api = new Api($api_key, $api_secret);

//Add Payment

$data = [];
$data['firstname']   = $_POST['firstname'];
$data['lastname']    = $_POST['lastname'];
$data['email']       = $_POST['email'];
$data['phone']       = $_POST['phone'];
$data['amount']      = $_POST['amount'];
$data['currency']    = $_POST['currency'];
$data['description'] = $_POST['description'];
$data['address']     = $_POST['address'];
$data['message']     = $_POST['message'];
$data['feeType']     = $_POST['feeType'];

$payment = $api->payment->addPayment($data);

//Check Balance 

$check_balance = $api->balance->check_balance();

//Check Payment
$check_payment = $api->payment->check_payment($tranID);





# Prerequisites:

To obtain the Secret Key,Web Hook and Hash, Please register yourself onto the  www.moneyspace.net 
Input the Domain and the site will generate the Secret Id,Secret Key,Web Hook.

 
#Format for  various API(s) as follows:


----Create Payment----

### Input
$data['firstname']   = $_POST['firstname'];
$data['lastname']    = $_POST['lastname'];
$data['email']       = $_POST['email'];
$data['phone']       = $_POST['phone'];
$data['amount']      = $_POST['amount'];
$data['currency']    = $_POST['currency']; -- This should be THB
$data['description'] = $_POST['description'];
$data['address']     = $_POST['address'];
$data['message']     = $_POST['message'];
$data['feeType']     = $_POST['feeType'];

Example Parameter
		secreteID = 58OF6L2CN75Y1298E09M
		firstname = example
		lastname = example
		email = example@example.com
		phone = 123456789
		amount = 100.00
		currency = THB
		description = example
		address = 171/1
		message = Test
		feeType = include
		timeHash = 20180918133421
		customer_order_id = 1
		successUrl = www.test.com
		failUrl = www.test.com
		cancelUrl = www.test.com
		hash = 3cff44754c288ecf43c54d9af37f3b358e1a0f324728fd82dd749278980c1aa8	
	
      

---- To check the Balance----
check_balance()
    {
    	$key          = parent::getKey();
    	$getSecret    = parent::getSecret();
    	$getTime      = parent::getTime();
    	$dataHash     = parent::getHash($getTime, $key);
		$request 	  = 'secreteID=' . $getSecret . '&timeHash=' . $getTime . '&hash=' . $dataHash;
    	$url          = parent::getFullUrl('/createpayment/obj');
    	$getConection = parent::getConection($url ,$request);