<?php

namespace Moneyspace\Api;

class Api
{
    protected static $baseUrl = 'https://www.moneyspace.net/merchantapi/v1';

    protected static $key = null;

    protected static $secret = null;

    public static $appsDetails = array();

    const VERSION = '1.0.0';

    /**
     * @param string $key
     * @param string $secret
     */
    public function __construct($key = null, $secret = null)
    {
        if ( !empty($key) && !empty($secret) ) {
            self::$key = $key;
            self::$secret = $secret;
        }
    }

    /*
     *  Set Headers
     */
    public function setHeader($header, $value)
    {
        Request::addHeader($header, $value);
    }

    public function setAppDetails($title, $version = null)
    {
        $app = array(
            'title' => $title,
            'version' => $version
        );

        array_push(self::$appsDetails, $app);
    }

    public function getAppsDetails()
    {
        return self::$appsDetails;
    }

    public function setBaseUrl($baseUrl)
    {
        self::$baseUrl = $baseUrl;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    { 
        $className = __NAMESPACE__.'\\'.ucwords($name);

        $entity = new $className();

        return $entity;
    }

    public static function getBaseUrl()
    {
        return self::$baseUrl;
    }

    public static function getKey()
    {
        return self::$key;
    }

    public static function getSecret()
    {
        return self::$secret;
    }

    public static function getFullUrl($relativeUrl)
    {
        return self::getBaseUrl() . $relativeUrl;
    }

    public function getConection($url, $request) {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
        curl_setopt($ch, CURLOPT_HEADER, 0); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch); 
        curl_close($ch); 
        return $response;
    }

    public function getHash($data,$key) {
        return hash_hmac('sha256', $data, $key);
    }

    public function getTime() {
        return date("YmdHis");
    }
}