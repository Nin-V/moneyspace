<?php 
require_once "Require.php";

use Moneyspace\Api\Api;

$api = new Api('API key','API secret key');

if(!empty($_POST)){

	if($_POST['action'] == 'addpayment'){
		$data = [];
		$data['firstname']   = $_POST['firstname'];
		$data['lastname']    = $_POST['lastname'];
		$data['email']       = $_POST['email'];
		$data['phone']       = $_POST['phone'];
		$data['amount']      = $_POST['amount'];
		$data['currency']    = $_POST['currency'];
		$data['description'] = $_POST['description'];
		$data['address']     = $_POST['address'];
		$data['message']     = $_POST['message'];
		$data['feeType']     = $_POST['feeType'];

		$payment = $api->payment->addPayment($data);

		print( json_encode($payment));
	}
	else if($_POST['action'] == 'checkbalance'){
		$check_balance = $api->balance->check_balance();
		print( json_encode($check_balance));
	}
	else if($_POST['action'] == 'checkpayment'){
		$check_payment = $api->payment->check_payment('MSTRF18000XXXXXX');
		print( json_encode($check_payment));
	}
}