<?php

namespace Moneyspace\Api;

class Payment extends Api
{

	public function __construct($key = null, $secret = null)
    {
    	parent::__construct();
    }

    public function addPayment($attributes = array())
    {

    	$key       = parent::getKey();
    	$getSecret = parent::getSecret();
    	$getTime   = parent::getTime();
    	$request   = 'secreteID='.$getSecret;
    	$dataHash  = '';

    	foreach ($attributes as $key => $value) {
    		$dataHash .= $value;
    	}

    	foreach ($attributes as $key => $value) {
    		$request .= '&'.$key.'='.$value;
    	}


    	$getHash      = parent::getHash($dataHash, $key);
    	$request 	 .= '&hash='.$getHash;
    	$url          = parent::getFullUrl('/createpayment/obj');
    	$getConection = parent::getConection($url ,$request);

    	return $getConection;
    }

    public function check_payment($tranID)
    {
    	$key          = parent::getKey();
    	$getSecret    = parent::getSecret();
    	$getTime      = parent::getTime();
    	$dataHash     = parent::getHash($tranID.$getTime, $key);
		$request 	  = 'secreteID=' . $getSecret . '&timeHash=' . $getTime . '&hash=' . $dataHash;
		$request 	  = 'secreteID=' . $getSecret . '&transactionID=' . $tranID . '&timeHash=' . $getTime . '&hash=' . $dataHash;
    	$url          = parent::getFullUrl('/createpayment/obj');
    	$getConection = parent::getConection($url ,$request);

    	return $getConection;

    }
}