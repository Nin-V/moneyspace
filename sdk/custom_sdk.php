<?php 
require_once 'Api.php';
require_once 'Balance.php';
require_once 'Payment.php';

use sdk\Api\Api;

$api = new Api();
if($_POST['action'] == 'addpayment'){
	$data = [];
	$data['firstname']   = $_POST['firstname'];
	$data['lastname']    = $_POST['lastname'];
	$data['email']       = $_POST['email'];
	$data['phone']       = $_POST['phone'];
	$data['amount']      = $_POST['amount'];
	$data['currency']    = $_POST['currency'];
	$data['description'] = $_POST['description'];
	$data['address']     = $_POST['address'];
	$data['message']     = $_POST['message'];
	$data['feeType']     = $_POST['feeType'];

	$payment = $api->payment->addPayment($data);

	print( json_encode($payment));
}
else if($_POST['action'] == 'checkbalance'){
	$check_palance = $api->balance->check_balance();
	print( json_encode($check_palance));
}
else if($_POST['action'] == 'checkpayment'){
	$check_payment = $api->payment->check_payment('MSTRF18000XXXXXX');
	print( json_encode($check_payment));
}