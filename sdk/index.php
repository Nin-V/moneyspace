<!DOCTYPE html>
<html>
<head>
	<title>SDK</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class='container'>
		<div class='row'>
			<div class='col-md-4 col-md-offset-4'>
				<form id="payment_form" method='post' action="Moneyspace.php">
				  <div class="form-group">
				    <label for="firstname">First Name:</label>
				    <input type="text" name = "firstname" class="form-control" id="firstname">
				  </div>
				  <div class="form-group">
				    <label for="lastname">Last Name:</label>
				    <input type="text" name = "lastname" class="form-control" id="lastname">
				  </div>
				  <div class="form-group">
				    <label for="email">Email address:</label>
				    <input type="email" name = "email"  class="form-control" id="email">
				  </div>
				  <div class="form-group">
				    <label for="phone">Phone:</label>
				    <input type="text" name = "phone"  class="form-control" id="phone">
				  </div>
				  <div class="form-group">
				    <label for="amount">Amount:</label>
				    <input type="text" name = "amount"  class="form-control" id="amount">
				  </div>
				  <div class="form-group">
				    <label for="currency">Currency:</label>
				    <input type="text" name = "currency"  class="form-control" id="currency">
				  </div>
				  <div class="form-group">
				    <label for="description">Description:</label>
				    <input type="text" name = "description"  class="form-control" id="description">
				  </div>
				  <div class="form-group">
				    <label for="address">Address:</label>
				    <input type="text" name = "address"  class="form-control" id="address">
				    <input type="hidden" name = "action" value='addpayment'>
				  </div>
				  <div class="form-group">
				    <label for="message">Message:</label>
				    <input type="text" name = "message"  class="form-control" id="message">
				  </div>
				  <div class="form-group">
				    <label for="feeType">FeeType:</label>
				    <input type="text" name = "feeType" class="form-control" id="feeType">
				  </div>
				  <button type="submit" class="btn btn-default ">Add Payment</button>
				</form><br>
				  <button type="submit" class="btn btn-primary checkbalance">Check Balance</button>
				  <button type="submit" class="btn btn-success checkpayment">Check Payment</button><br><br>
				<div class='alert alert-danger result' style='display:none'>
					s
				</div>
			</div>
		</div>
	</div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('submit','#payment_form',function(e){
			e.preventDefault();
			var data = $(this).serializeArray();

			$.ajax({
				type:'post',
				url:"Moneyspace.php",
				dataType:'json',
				data:data,
				success:function(res){
					$(".result").slideDown(200);
					res = JSON.parse(res);
					$(".result").text( res[0].status );
				}
			})
		})
		$(document).on('click','.checkbalance',function(){
			$.ajax({
				type:'post',
				url:"Moneyspace.php",
				dataType:'json',
				data:{action:"checkbalance"},
				success:function(res){
					$(".result").slideUp(200);
					$(".result").slideDown(200);
					res = JSON.parse(res);
					$(".result").text(" Your Balance status is : " + res[0].status);
				}
			})
		})
		$(document).on('click','.checkpayment',function(){
			$.ajax({
				type:'post',
				url:"Moneyspace.php",
				dataType:'json',
				data:{action:"checkpayment"},
				success:function(res){
					$(".result").slideUp(200);
					$(".result").slideDown(200);
					res = JSON.parse(res);
					$(".result").text("Your Payment status is : " + res[0].status);
				}
			})
		})
	})
</script>
</html>